# App [![pipeline status](https://gitlab.com/diamn/test-drweb/badges/master/pipeline.svg)](https://gitlab.com/diamn/test-drweb/-/commits/master)

## Сборка проекта:
```bash
python setup.py bdist_wheel
```

## Запуск тестов:
```bash
pip install -r requirements/development.txt
pytest tests --cov package
```
